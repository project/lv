# LANGUAGE translation of Drupal (modules/tracker.module)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from file: tracker.module,v 1.129 2006/04/17 20:48:26 dries
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2006-06-05 17:49+0200\n"
"PO-Revision-Date: 2006-09-12 22:16+0200\n"
"Last-Translator: Dita <dita@limbo.lv>\n"
"Language-Team: DG <dita@limbo.lv>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2;\n"
"X-Poedit-Language: Latvian\n"
"X-Poedit-Country: Latvia\n"

#: modules/tracker.module:15
msgid "The tracker module displays the most recently added or updated content to the website allowing users to see the most recent contributions.  The tracker module provides user level tracking for those who like to follow the contributions of particular authors."
msgstr "Sekotāja modulis attēlo jaunāko pievienoto vai papildināto saturu tīmekļa vietnē, ļaujot lietotājiem redzēt jaunāko devumu. Sekotāja modulis nodrošina sekošanu lietotājam, tas ir noderīgi tiem, kam patīk lasīt kāda noteikta autora pievienoto saturu."

#: modules/tracker.module:16
msgid "The  &quot;recent posts&quot; page is available via a link in the navigation menu block and contains a reverse chronological list of new and recently-updated content. The table displays  the content type, the title, the author's name, how many comments that item has received, and when it was last updated. Updates include any changes to the text, either by the original author or someone else, as well as any new comments added to an item.  To use the tracker module to <em>watch</em> for a user's updated content, click on that user's profile, then the <em>track</em> tab."
msgstr "&quot;recent posts&quot; lapa ir pieejama no norādes navigācijas izvēlnes blokā, tā satur jauno vai mainīto saturu apgrieztā hronoloģiskā secībā. Tabulā ir satura veids, virsraksts, autors, komentāru skaits un kad tas pēdējo reizi mainīts. Izmaiņas iekļauj jebkuras izmaiņas tekstā, kuras veicis vai nu pats autors, vai jebkurš cits, kā arī jauna komentāra pievienošanu tekstam. Lai izmantotu sekotāja moduli, lai <em>sekotu</em> noteikta lietotāja mainītajam saturam, uzklikšķiniet uz lietotāj profila un pēc tam us sadaļas <em>sekot</em>."

#: modules/tracker.module:17
msgid ""
"<p>You can</p>\n"
"<ul>\n"
"<li>view the <a href=\"%tracker\">most recent posts</a>.</li>\n"
"<li>view <a href=\"%profile\">user profiles</a> and select the track tab.</li>\n"
"<li>not administer this module.</li>\n"
"</ul>\n"
msgstr ""
"<p>Jūs varat</p>\n"
"<ul>\n"
"<li>redzēt <a href=\"%tracker\">jaunākās ievietotās ziņas</a>.</li>\n"
"<li>redzēt <a href=\"%profile\">lietotāja profilus</a> un izvēlēties sadaļu &quot;Sekot&quot;.</li>\n"
"<li>neadministrēt šo moduli.</li>\n"
"</ul>\n"

#: modules/tracker.module:24
msgid "For more information please read the configuration and customization handbook <a href=\"%tracker\">Tracker page</a>."
msgstr "Lai iegūtu vairāk informācijas, lūdzu, lasiet konfigurēšanas un pielāgošanas rokasgrāmatu <a href=\"%tracker\">Sekotāja lapā</a>."

#: modules/tracker.module:27
msgid "Enables tracking of recent posts for users."
msgstr "Atļauj sekot lietotāju ievietotajām jaunākajām ziņām."

#: modules/tracker.module:39
msgid "recent posts"
msgstr "jaunākās ievietotās ziņas"

#: modules/tracker.module:44
msgid "all recent posts"
msgstr "visas jaunākās ziņas"

#: modules/tracker.module:46
msgid "my recent posts"
msgstr "manas jaunākās ziņas"

#: modules/tracker.module:55
msgid "track posts"
msgstr "sekot ievietotajām ziņām"

#: modules/tracker.module:121
msgid "Post"
msgstr "Ievietotā ziņa"

#: modules/tracker.module:0
msgid "tracker"
msgstr "sekotājs"

