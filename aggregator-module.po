# LANGUAGE translation of Drupal (modules/aggregator.module)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from file: aggregator.module,v 1.278.2.2 2006/05/04 18:45:18 killes
#
msgid ""
msgstr ""
"Project-Id-Version: DRUPAL-4-7\n"
"POT-Creation-Date: 2006-06-05 17:49+0200\n"
"PO-Revision-Date: 2006-09-26 16:48+0200\n"
"Last-Translator: Dita <dita@limbo.lv>\n"
"Language-Team: Dita Gabalina, Miķelis Zaļais <dita@limbo.lv>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Latvian\n"
"X-Poedit-Country: LATVIA\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : 2;\n"

#: modules/aggregator.module:15
msgid "The news aggregator is a powerful on-site RSS syndicator/news reader that can gather fresh content from news sites and weblogs around the web."
msgstr "Jaunumu apkopotājs ir spēcīgs on-site RSS syndicator/jaunumu lasītājs, kas var iegūt jaunāko saturu no visa tīmekļa jaunumu lapām un webžurnāliem."

#: modules/aggregator.module:16
msgid "Users can view the latest news chronologically in the <a href=\"%aggregator\">main news aggregator display</a> or by <a href=\"%aggregator-sources\">source</a>. Administrators can add, edit and delete feeds and choose how often to check for newly updated news for each individual feed. Administrators can also tag individual feeds with categories, offering selective grouping of some feeds into separate displays. Listings of the latest news for individual sources or categorized sources can be enabled as blocks for display in the sidebar through the <a href=\"%admin-block\">block administration page</a>. The news aggregator requires cron to check for the latest news from the sites to which you have subscribed. Drupal also provides a <a href=\"%aggregator-opml\">machine-readable OPML file</a> of all of your subscribed feeds."
msgstr "Lietotāji var redzēt pēdējos jaunumus hronoloģiskā secībā <a href=\"%aggregator\">galvenajā jaunumu apkopotāja lapā</a> vai arī pēc <a href=\"%aggregator-sources\">avota</a>. Administratori var pievienot, rediģēt un dzēst barotnes, kā arī norādīt, cik bieži pārbaudīt jaunumu esamību katrā individuālā barotnē. Administratori var barotnēm pievienot kategorijas, tādējādi grupējot tās īpašās lapās. Atsevišķiem avotiem vai avotiem ar noteiktām kategorijām, jaunāko ziņu saraksti var tikt veidoti kā bloki, lai tos attēlotu sānjoslās. To var norādīt  <a href=\"%admin-block\">bloka adminstrācijas lapā</a>. Jaunumu agregators prasa cron esamību, lai pārbaudītu jaunumu esamību vietnēs, uz kurām jūs esiet parakstījušies. Drupal nodrošina  arī <a href=\"%aggregator-opml\">mašīnlasāmu OPML failu</a> visām jūsu abonētajām barotnēm."

#: modules/aggregator.module:17
msgid ""
"<p>You can</p>\n"
"<ul>\n"
"<li>administer your list of news feeds <a href=\"%admin-aggregator\">administer &gt;&gt;  aggregator</a>.</li>\n"
"<li>add a new feed <a href=\"%admin-aggregator-add-feed\">administer &gt;&gt; aggregator &gt;&gt; add feed</a>.</li>\n"
"<li>add a new category <a href=\"%admin-aggregator-add-category\">administer &gt;&gt; aggregator &gt;&gt; add category</a>.</li>\n"
"<li>configure global settings for the news aggregator <a href=\"%admin-settings-aggregator\">administer &gt;&gt; settings &gt;&gt; aggregator</a>.</li>\n"
"<li>control access to the aggregator module through access permissions <a href=\"%admin-access\">administer &gt;&gt; access control &gt;&gt; permissions</a>.</li>\n"
"<li>set permissions to access new feeds for user roles such as anonymous users at <a href=\"%admin-access\">administer &gt;&gt; access control</a>.</li>\n"
"<li>view the <a href=\"%aggregator\">aggregator page</a>.</li>\n"
"</ul>\n"
msgstr ""
"<p>Jūs varat</p>\n"
"<ul>\n"
"<li>administrēt jūsu sarakstu ar jaunumu barotnēm<a href=\"%admin-aggregator\">administrēt &gt;&gt;  apkopotāju</a>.</li>\n"
"<li>pievienot jaunu barotni <a href=\"%admin-aggregator-add-feed\">administrēt &gt;&gt; jaunumu apkopotājs &gt;&gt; pievienot barotni</a>.</li>\n"
"<li>pievienot jaunu kategoriju <a href=\"%admin-aggregator-add-category\">administrēt &gt;&gt; jaunumu apkopotājs &gt;&gt; pievienot kategoriju</a>.</li>\n"
"<li>konfigurēt kopējos uzstādījumus jaunumu agregatoram <a href=\"%admin-settings-aggregator\">administrēt &gt;&gt; uzstādījumi &gt;&gt; apkopotājs</a>.</li>\n"
"<li>kontrolēt pieeju agregatora moduli izmantojot pieejas tiesības <a href=\"%admin-access\">administrēt &gt;&gt; pieejas tiesības &gt;&gt; atļaujas</a>.</li>\n"
"<li>atļaut piekļūt jaunām barotnēm lietotāju lomām, piemēram, anonīmiem lietotājiem <a href=\"%admin-access\">administrēt &gt;&gt; pieejas tiesības</a>.</li>\n"
"<li>pārskatīt <a href=\"%aggregator\">jaunumu savācēja lapu</a>.</li>\n"
"</ul>\n"

#: modules/aggregator.module:28
msgid "For more information please read the configuration and customization handbook <a href=\"%aggregator\">Aggregator page</a>."
msgstr "Lai iegūtu vairāk informācijas, lūdzu, izlasiet konfigurēšanas un pielāgošanas rokasgrāmatu <a href=\"%help\">Jaunumu apkopotāja lapā</a>."

#: modules/aggregator.module:31
msgid "Aggregates syndicated content (RSS, RDF, and Atom feeds)."
msgstr "Apkopo sindicēto saturu (RSS, RDF, un Atom barotnes)."

#: modules/aggregator.module:33
msgid "<p>Thousands of sites (particularly news sites and weblogs) publish their latest headlines and/or stories in a machine-readable format so that other sites can easily link to them. This content is usually in the form of an <a href=\"http://blogs.law.harvard.edu/tech/rss\">RSS</a> feed (which is an XML-based syndication standard). To display the feed or category in a block you must decide how many items to show by editing the feed or block and turning on the <a href=\"%block\">feed's block</a>.</p>"
msgstr "<p>Tūkstošiem vietnes (it īpaši jaunumu un webžurnālu vietnes) publicē pēdējos ziņu virsrakstus un rakstus mašīnlasāmā formātā, lai citas vietnes varētu viegli uz tām norādīt. Šis saturs parasti ir formā <a href=\"http://blogs.law.harvard.edu/tech/rss\">RSS</a> barotne (XML-bāzēts based apmaiņas standarts). Lai attēlotu barotni vai kategoriju blokā, Jums jāizlemj, cik vienības rādīt, rediģējot barotni vai bloku un ieslēdzot <a href=\"%block\">barotnes bloku</a>.</p>"

#: modules/aggregator.module:35
msgid "<p>Add a site that has an RSS/RDF/Atom feed. The URL is the full path to the feed file. For the feed to update automatically you must run \"cron.php\" on a regular basis. If you already have a feed with the URL you are planning to use, the system will not accept another feed with the same URL.</p>"
msgstr "<p>Pievienojiet vietni, kura ir RSS/RDF/Atom barotne.  URL ir pilns ceļš uz barotnes failu. Lai barotne papildinātos automātiski, jums regulāri jādarbina  \"cron.php\". Ja Jums jau ir kāds URL ar barotni, ko plānojat izmantot, sistēma nepieņems citu barotni ar tādu pat URL. </p>"

#: modules/aggregator.module:37
msgid "<p>Categories provide a way to group items from different news feeds together. Each news category has its own feed page and block. For example, you could tag various sport-related feeds as belonging to a category called <em>Sports</em>. News items can be added to a category automatically by setting a feed to automatically place its item into that category, or by using the categorize items link in any listing of news items.</p>"
msgstr "<p>Kategorijas nodrošina veidu, kā grupēt vienības no dažādām jaunumu barotnēm. Katrai jaunumu kategorijai ir sava barotnes lapa un bloks. Piemēram, jūs varat pievienot  etiķeti <em>Sports</em>dažādām ar sportu saistītām aktivitātēm. Jaunumu vienības var automātiski pievienot kategorijai, barotnei norādot automātiski pievienot vienību šai kategorijai vai arī izmantojot vienību kategorijas norādi jebkurā no jaunumu sarakstiem.</p>"

#: modules/aggregator.module:51;1194;374;385;507;518;707;756;761;818;0
msgid "aggregator"
msgstr "jaunumu apkopotājs "

#: modules/aggregator.module:55
msgid "add feed"
msgstr "pievienot barotni"

#: modules/aggregator.module:65;970
msgid "remove items"
msgstr "aizvākt vienības"

#: modules/aggregator.module:70;970
msgid "update items"
msgstr "labot vienības"

#: modules/aggregator.module:80
msgid "news aggregator"
msgstr "jaunumu apkopotājs"

#: modules/aggregator.module:85
msgid "sources"
msgstr "avoti"

#: modules/aggregator.module:94
msgid "RSS feed"
msgstr "RSS barotne"

#: modules/aggregator.module:99
msgid "OPML feed"
msgstr "OPML barotne"

#: modules/aggregator.module:127;148
msgid "categorize"
msgstr "pievienot kategorijas"

#: modules/aggregator.module:167
msgid "edit feed"
msgstr "rediģēt barotni"

#: modules/aggregator.module:178
msgid "edit category"
msgstr "rediģēt katgoriju"

#: modules/aggregator.module:201
msgid "The list of tags which are allowed in feeds, i.e., which will not be removed by Drupal."
msgstr "Tagu (etiķešu) saraksts, kuri nav pieļaujami barotnēs, un kurus Drupal noņems."

#: modules/aggregator.module:205
msgid "Items shown in sources and categories pages"
msgstr "Vienības, kuras rādīt avotu un kategoriju lapās"

#: modules/aggregator.module:207
msgid "The number of items which will be shown with each feed or category in the feed and category summary pages."
msgstr "Vienību skaits, kuras tiks rādītas katrai barotnei vai kategorijai barotnes un kategorijas kopsavilkuma lapās."

#: modules/aggregator.module:211
msgid "Discard news items older than"
msgstr "Aizvākt vienības, vecākas kā"

#: modules/aggregator.module:213
msgid "Older news items will be automatically discarded.  Requires crontab."
msgstr "Vecākas vienības tiks automātiski aizvāktas. Nepieciešams crontab."

#: modules/aggregator.module:217
msgid "Category selection type"
msgstr "Kategoriju izvēles veids"

#: modules/aggregator.module:218
msgid "checkboxes"
msgstr "čekboksi"

#: modules/aggregator.module:218
msgid "multiple selector"
msgstr "daudzkārtēja izvēle"

#: modules/aggregator.module:219
msgid "The type of category selection widget which is shown on categorization pages. Checkboxes are easier to use; a multiple selector is good for working with large numbers of categories."
msgstr "Kategorijas izvēles logrīks (widget), kuru parāda kategoriju piešķiršanas lapās. Čekboksus ir vieglāk izmantot; savukārt multiizvēles saraksts ir noderīgs gariem kategoriju sarakstiem."

#: modules/aggregator.module:253
msgid "%title category latest items"
msgstr "Kategorijas %title jaunākās vienības"

#: modules/aggregator.module:257
msgid "%title feed latest items"
msgstr "Barotnes %title jaunākās vienības"

#: modules/aggregator.module:268
msgid "Number of news items in block"
msgstr "Jaunumu vienību skaits blokā"

#: modules/aggregator.module:287
msgid "View this feed's recent news."
msgstr "Skatīt šīs barotnes jaunākās ziņas."

#: modules/aggregator.module:295
msgid "View this category's recent news."
msgstr "Skatīt šīs kategorijas jaunākās ziņas."

#: modules/aggregator.module:346
msgid "A category named %category already exists. Please enter a unique title."
msgstr "Katgorija %category jau eksistē. Lūdzu, norādiet unikālu nosaukumu."

#: modules/aggregator.module:365
msgid "The category %category has been updated."
msgstr "Kategorija  %category izlabota."

#: modules/aggregator.module:374
msgid "Category %category deleted."
msgstr "Kategorija %category dzēsta."

#: modules/aggregator.module:375
msgid "The category %category has been deleted."
msgstr "Kategorija %category dzēsta."

#: modules/aggregator.module:385
msgid "Category %category added."
msgstr "Kategorija %category pievienota."

#: modules/aggregator.module:386
msgid "The category %category has been added."
msgstr "Kategorija %category pievienota."

#: modules/aggregator.module:421
msgid "The name of the feed; typically the name of the web site you syndicate content from."
msgstr "Barotnes nosaukums, parasti tīmekļa vietnes nosaukums, no kura saņem sindicētu saturu."

#: modules/aggregator.module:428
msgid "The fully-qualified URL of the feed."
msgstr "Pilnībā aizpildīts URL barotnei."

#: modules/aggregator.module:432
msgid "Update interval"
msgstr "Labošanas intervāls"

#: modules/aggregator.module:435
msgid "The refresh interval indicating how often you want to update this feed. Requires crontab."
msgstr "Atjaunošanas intervāls, kas norāda, cik bieži jūs vēlaties atjaunot barotni. Nepieciešams crontab."

#: modules/aggregator.module:448
msgid "Categorize news items"
msgstr "Jaunumus pievienot kategorijām"

#: modules/aggregator.module:451
msgid "New items in this feed will be automatically filed in the checked categories as they are received."
msgstr "Jaunumi, tos saņemot, šajā barotnē tiks automātiski pievienoti norādītajām kategorijām."

#: modules/aggregator.module:478
msgid "A feed named %feed already exists. Please enter a unique title."
msgstr "Barotne %feed jau eksistē. Lūdzu, norādiet unikālu nosaukumu. "

#: modules/aggregator.module:498
msgid "The feed %feed has been updated."
msgstr "Barotne %feed izlabota."

#: modules/aggregator.module:507
msgid "Feed %feed deleted."
msgstr "Barotne %feed dzēsta."

#: modules/aggregator.module:508
msgid "The feed %feed has been deleted."
msgstr "Barotne %feed dzēsta."

#: modules/aggregator.module:518
msgid "Feed %feed added."
msgstr "Barotne %feed pievienota."

#: modules/aggregator.module:519
msgid "The feed %feed has been added."
msgstr "Barotne %feed pievienota."

#: modules/aggregator.module:572
msgid "The news items from %site have been removed."
msgstr "Jaunumi no vietnes %site aizvākti."

#: modules/aggregator.module:703
msgid "There is no new syndicated content from %site."
msgstr "Nav jauna sindicētā satura no %site."

#: modules/aggregator.module:707
msgid "Updated URL for feed %title to %url."
msgstr "Labots URL barotnei %title uz %url."

#: modules/aggregator.module:756;757
msgid "There is new syndicated content from %site."
msgstr "Nav jauna sindicēta satura no %site."

#: modules/aggregator.module:761
msgid "The RSS-feed from %site seems to be broken, due to \"%error\"."
msgstr "RSS-barotne no %site izskatās salauzta, jo ir kļūdas \"%error\"."

#: modules/aggregator.module:762
msgid "The RSS-feed from %site seems to be broken, because of error \"%error\"."
msgstr "RSS-barotne no %site izskatās salauzta, jo ir kļūdas \"%error\"."

#: modules/aggregator.module:818
msgid "The RSS-feed from %site seems to be broken, due to an error \"%error\" on line %line."
msgstr "RSS-barotne no %site izskatās salauzta, jo ir kļūdas \"%error\" rindā %line."

#: modules/aggregator.module:819
msgid "The RSS-feed from %site seems to be broken, because of error \"%error\" on line %line."
msgstr "RSS-barotne no %site izskatās salauzta, jo ir kļūdas \"%error\" rindā %line."

#: modules/aggregator.module:965
msgid "Feed overview"
msgstr "Barotnes pārskats"

#: modules/aggregator.module:967;978
msgid "Items"
msgstr "Vienības"

#: modules/aggregator.module:967
msgid "Last update"
msgstr "Pēdējais papildinājums"

#: modules/aggregator.module:967
msgid "Next update"
msgstr "Nākamais papildinājums"

#: modules/aggregator.module:970
msgid "%time left"
msgstr "Atlicis %time"

#: modules/aggregator.module:976
msgid "Category overview"
msgstr "Kategoriju pārskats"

#: modules/aggregator.module:1077
msgid "Save categories"
msgstr "Saglabāt kategorijas"

#: modules/aggregator.module:1105
msgid "Categorize"
msgstr "Piešķirt kategoriju"

#: modules/aggregator.module:1114
msgid "You are not allowed to categorize this feed item."
msgstr "Jums nav atļauts piešķirt kategoriju šai barotnes vienībai."

#: modules/aggregator.module:1127
msgid "The categories have been saved."
msgstr "Kategorijas saglabātas."

#: modules/aggregator.module:1166
msgid "in category"
msgstr "kategorijā"

#: modules/aggregator.module:1194
msgid "aggregated feeds"
msgstr "apkopotas barotnes"

#: modules/aggregator.module:1265
msgid "URL:"
msgstr "URL:"

#: modules/aggregator.module:1278
msgid "Updated:"
msgstr "Labots:"

#: modules/aggregator.module:1293;1293
msgid "blog it"
msgstr "ierakstīt emuāros"

#: modules/aggregator.module:1294
msgid "Comment on this news item in your personal blog."
msgstr "Komentēt par šo ziņu jūsu personīgajos emuāros."

#: modules/aggregator.module:1314
msgid "%age old"
msgstr " vecs: %age"

#: modules/aggregator.module:1334
msgid "%ago ago"
msgstr "pirms %ago"

#: modules/aggregator.module:970;981;1373
msgid "1 item"
msgid_plural "%count items"
msgstr[0] "1 vienība"
msgstr[1] "%count vienības"
msgstr[2] "nav vienību"

#: modules/aggregator.module:228
msgid "administer news feeds"
msgstr "administrēt jaunumu barotnes"

#: modules/aggregator.module:228
msgid "access news feeds"
msgstr "piekļūt jaunumu barotnēm"

